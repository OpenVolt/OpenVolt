#!/usr/bin/env nix-shell
# NixOS shell configuration to bootstrap the required dependencies and start the editor

with import <nixpkgs> {};
stdenv.mkDerivation {
  name = "OpenVolt";
	buildInputs = [
		pkgs.vscodium-fhs
	];
}
