# Single Sign-On (SSO) considerations

Documment tracking pros/cons to determine the best candidate to handle SSO

## Requirement checklist
[MANDATORY] Complies with Four Freedoms of Free Software
[+50000] registration
[+50000] account recovery
[+50000] TechnoLiberal
[+5000] Supports Proof-of-Work
[+5000] Source code went through security audit
[+5000] Multi-Factor Authentification (MFA)
[+5000] Zero-Knowledg
[+5000] Adapted for Fedarated networks
[+5000] Works with Onion Router (Tor and LOKI)
[+5000] Allows to cherrypick user access per service
[+1000] Does NOT require e-mail for authentification
[-5000] Mandatory phone number use
[???] identity schemas
[???] headless

Social sign-in:
[+5000] Facebook
[+5000] Twitter
[+5000] GitHub
[+5000] Apple
[+5000] GitLab

Programming language:
[+5000] Resource efficient programming language
- [+1000] Rustlang
- [+900] GoLang
- [+500] C
- [+500] V (requires security checks)
- [UNUSABLE] C++ (depending on the build system)
- [UNUSABLE] Java
- [UNUSABLE] C#
- [???] PHP

Service checks.. Works with:
[+1000] Gitea
[+1000] Works with Nextcloud
[+1000] Works with Discourse
[+1000] Works with Invidious
[+250] Works with Bibliogram
[+250] Works with Nitter
[+250] Works with RoundCube

Packaged and maintained by NixOS:
[+20000] Active maintainers

----

### Kratos
https://github.com/ory/kratos

```
[+300] GoLang
[+5000] Packaged and maintained by NixOS
[???] Ory-hardened authentication
[+5000] MFA
[???] FIDO2
[???] profile management
----
XXX
```

### GoTrue
https://github.com/netlify/gotrue

```
----
```

### oauth2-server
https://github.com/thephpleague/oauth2-server

```
[+200] Written in PHP
----
```

### PWM
https://github.com/pwm-project/pwm

```
[UNUSABLE] Written in Java
----
DISQUALIFIED
```

### KeyCloak
https://www.keycloak.org
```
[UNUSABLE] Written in Java
[+5000] Packaged and maintained by NixOS
----
DISQUALIFIED
```

### SSO crate (rustlang)
https://mojzu.net/sso/

```
[-5000] Abadonware
[+6000] Rustlang
[-5000] Maintainer disclaims lack of experience in security
[+300] Can be self-maintained
[+500] Code seems usable
[-1000] Requires work
-----
DISQUALIFIED
```