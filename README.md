![](img/OpenVolt_repo_header.png)

# OpenVolt CSE, w.i.p.

Pending policy initiative in VoltEuropa ("party") created by Jacob "Kreyren" Hrbek that empowers volters ("registered volt members") and general public to engage in a transparent and inclusive political process to influence party's policies through providing a new perspective and ideas while making the party independent from current BigTech influence.

We do this by commiting to the **[Free/Libre Open-Source software](https://fsfe.org/freesoftware/)** ("FLOSS") while preserving **[TechnoLiberal](https://en.wikipedia.org/wiki/Technoliberalism)** principles, through adaptation of **[Linus's Law](https://en.wikipedia.org/wiki/Linus%27s_law)** and strict privacy and ecological policies that grants everyone the equal opportunities in the party and ability to efficiently propose new changes and policies.

## Fundamental pillars

This initiative is built upon these fundamental pillars which values are non-negotiable.

We urge our supporters to hold this initiative accountable and to enforce these values.

### Ecology

The global warming is crisis that threatens the survival of humanity on Earth.{REF Needed}

{FIXME(Krey): Ecological strategy yet to be defined}

To address this issue internally we use services and servers that run on clean energy and/or contribute to the solution of global warming.

Certificates for server that we use:
- https://www.hetzner.com/assets/Uploads/Oomi-sertifikaatti-tuuli+vesi-Hetzner-2021.pdf
- https://www.hetzner.com/assets/Uploads/downloads/oekostrom-zertifikat-2022.pdf

### Privacy

All user data can be abused to gain a destructive control over one's life which is why we see it as an essential human right for data subjects in question to have full control over their data and to know how exactly is the data processed and used.{REF NEEDED}

#### [Zero-Knowledge services and proof](https://en.wikipedia.org/wiki/Zero-knowledge_service)

We argue for a wide adaptation of Zero-Knowledge Services that encrypt the user data to allow the processing of without the service provider being able to decrypt them which enables the user to maintain full control over their privacy and only use the data for the designated purpose.

Videos on the subject:
A. https://redirect.invidious.io/watch?v=OcmvMs4AMbM&t=74
B. https://redirect.invidious.io/watch?v=HUs1bH85X9I?t=10

### Transparency

Due to the adaptation of **[Linus's Law](https://en.wikipedia.org/wiki/Linus%27s_law)** all software code (including infrastracture as code maintained through **[NixOS](https://nixos.org/)**) that is used within the initiative is subjected to public review and contribution on https://git.dotya.ml/OpenVolt following the principles of Free/Libre Open-Source Software.

All of our financing (including expenses) are published transparently on https://opencollective.com/openvolt

### Free/Libre Open-Source Software

FLOSS grants you the four essential rights (https://fsfe.org/freesoftware) to use the software for whatever purpose you want while enabling you to study how the software works which results in more functional, reliable and privacy respecting software by design.

#### Public code? Public Money!

is an initiative by The Free Software Foundation Europe that this project supports which aims to release the tax payer-funded governmental, educational and medical software as Free Software.

See https://publiccode.eu for more details

#### Software patents

Software should not be patentable, because it is inherently **[composed of mathematical logic](https://en.swpat.org/wiki/Software_is_math)**, which is excluded from **[patentable subject matter](https://en.swpat.org/wiki/Patentable_subject_matter)**.

In practice, the effects of software patents are so restrictive that they **[affect the development of all kinds of software, not just free software](https://libreplanet.org/wiki/Ciaran_O'Riordan_(LP09%29#Who's_affected_by_swpats))**.

See https://endsoftwarepatents.org for more informations

## References

1. The National Security Agency (NSA) of The United States of America spying the elected EU officials<br>
1.1. Report by The Guardian of NSA spying on the german counsellor Angela Merkel https://www.theguardian.com/us-news/2015/jul/08/nsa-tapped-german-chancellery-decades-wikileaks-claims-merkel<br>
1.2. Denmark accused of enabling NSA spying on EU officials https://www.bbc.com/news/world-europe-57302806
1.3. The german counsellor Angela Merkel testifying on the NSA probe https://www.dw.com/en/merkel-testifies-on-nsa-spying-affair/a-37576690

2. Threat of Software Paterns to FLOSS
2.1. FSF statement on the subject https://www.fsf.org/blogs/community/the-threat-of-software-patents-persists