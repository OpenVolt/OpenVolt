Copyright (C) Jacob Hrbek in 05/09/2021-EU

This license mimics the terms of GPLv3 <https://www.gnu.org/licenses/quick-guide-gplv3.en.html> while reserving the right to change the licensing at any point in the future for any reason.