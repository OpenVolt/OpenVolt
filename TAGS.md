## FIXME

For non-fatal issues that have to be addressed

## DND (DoNotDeploy)

Keyword for major issues used to block deployment of the configuration

#### SubTags

- QA = To address Code Quality
- REPRO = Issues that are not complying with reproducible environment
- SECURITY = To improve security
- FEDERATE = related to federation of the network
- TOR = Issues about The Onion Router (Tor)