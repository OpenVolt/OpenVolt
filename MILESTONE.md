# Development stages of this project

This highlights the current development effort and coordination on the project

## ALPHA
- [ ] Minimal Technical Requirements
	- [X] Abstract threat model
	- [X] Discourse
		- [X] Deployed
		- [X] Configured
	- [X] Nextcloud
		- [X] Deployed
		- [X] Configured
	- [X] Matrix
		- [X] Set up on tschnd.de
		- [X] Configured
	- [X] Gitea
		- [X] Set up on dotya.ml
		- [ ] Upload repo
	- [X] Website
		- [X] Deployed
		- [X] Configured
	- [X] Onion-Routing
		- [X] Tor
			- [X] Deployed
			- [X] Configured
	- [X] Deployment
		- [X] Code stabilized
		- [X] Sane for public review
	- [ ] Hotfix federation

- [ ] Minimal legal requirements
	- [ ] Constitution
	- [ ] Terms of Use
	- [ ] Disclaimer of Warranty
	- [ ] Transparent policy making

- [X] Minimal Economical requirements
	- [X] OpenCollective
		- [X] Configured
		- [X] Declared server budget

- [X] Optimal ecological requirements
	- [X] Use clean energy for all services

## BETA
- [ ] Optimal Technical requirements
	- [ ] Single Sign-On (SSO)
		- [ ] Deployed
		- [ ] Configured
		- [ ] Interface for user to login and configure services configured
	- [ ] Data sanity
	- [ ] Backups
	- [ ] Uptime guarantee
	- [ ] E-mail
	- [ ] Federation of services
	- [ ] Status page
	- [ ] Authoritative DNS Server
	- [ ] Grafana to publicly show system resources e.g. https://grafana.dotya.ml
	- [ ] Deploy services to serve for public good
		- [ ] Gitea
			- [ ] Deployed
			- [ ] Configured
		- [ ] SearX
			- [ ] Deployed
			- [ ] Configured
		- [ ] Invidious
			- [ ] Budget for Anti-Captha established
			- [ ] Deployed
			- [ ] Configured
		- [ ] Nitter
			- [ ] Deployed
			- [ ] Configured
		- [ ] Bibliogram
			- [ ] Deplyoed
			- [ ] Configured
		- [ ] LibReddit
			- [ ] Deployed
			- [ ] Configured
		- [ ] Jitsi Meet
			- [ ] Deployed
			- [ ] Configured
		- [ ] BigBlueButton
			- [ ] Deployed
			- [ ] Configured
		- [ ] Mastodon
			- [ ] Deployed
			- [ ] Configured

## RTFM
- [ ] Project considered successful

These criteria has to be met for this project to be considered successful:
	- [ ] VoltEuropa Fully migrated on technoliberal solutions with other deprecated
	- [ ] VoltEuropa adapted zero-knowledge solution for all members
	- [ ] VoltEuropa integrated policy that:
  	- [ ] FLOSS
  		- [X] Supports FLOSS
  		- [ ] Addresses software patent issue
  		- [ ] Enforces the use of FLOSS in
    		- [ ] public education institutions
    		- [ ] government institutions
  		- [ ] Protects FLOSS developers from legal harrasement (https://freespeech.firedragonstudios.com/web/statuses/106689601593541993)
    - [ ] Privacy
      - [ ] Reworks UKUSA agreenment
      - Please contribute...
    - [ ] Ecology
      - Please contribute...
    - [ ] Social equality
      - Please contribute...

## GOLD
- [ ] Project completed and declared successful
  - [ ] OpenVolt successfully merged in VoltEuropa